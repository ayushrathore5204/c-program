#include<stdio.h>
#include<conio.h>
//*   PRINT SIZE OF ALL VARIABLE 
int main()
{
	int intType;
	float floatType;
	double doubleType;
	char charType;
	long int longintType;
	long double longdoubleType; 
	
	printf(" Size of int variable : %d bytes \n", sizeof(intType));
	printf(" Size of float variable : %d bytes \n", sizeof(floatType));
	printf(" Size of double variable : %d bytes \n", sizeof(doubleType));
	printf(" Size of char variable : %d bytes \n", sizeof(charType));
	printf(" Size of long int variable : %d bytes \n", sizeof(longintType));
	printf(" Size of long double variable : %d bytes \n", sizeof(longdoubleType));
	getch();
}
