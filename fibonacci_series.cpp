#include<stdio.h>
#include<conio.h>
main()
{
	int number;
	
	// initialize 1st & 2nd terms
	int term1=0,term2=1;
	
	// initialize 3rd term(next term)	
	int NextTerm=term1+term2;
	
	// get input number to form user
	printf("Enter the number of term:");
	scanf("%d" , &number);
	
	//print the 1st 2 term of fibonacci series
	printf("Fibonacci Series : %d, %d, " , term1,term2);
	
	//print 3rd term to nth term
	for (int condition=3;condition<=number;++condition)
	{
		printf("%d, " , NextTerm);
		term1=term2;
		term2=NextTerm;
		NextTerm=term1 + term2;
	}
	getch();	
}
